class Node:
    def __init__(self, node_id, label):
        self.label = label
        self.node_id = node_id
        self.adj = []
        self.degree = 0
    def add_adj(self, edge):
        self.adj.append(edge)
        self.degree += 1
class Edge:
    def __init__(self, node_from, node_to, node_distance):
        self.node_from = node_from
        self.node_to = node_to
        self.distance = node_distance

def to_gephi(graph, mindegree = 0, maxdegree = 100):
    n = len(graph)
    
    prefix = datetime.datetime.now().strftime('%d%m%y_%H%M')
    
    edges_file = open('data/%s_edges.csv' % prefix, 'w')
    edges_file.write('Id,Label,Source,Target,Type,Weight\n')

    node_file = open('data/%s_nodes.csv' % prefix, 'w')
    node_file.write('Id,Label\n')

    min_degree = 0
    max_degree = 100

    ctr = 0

    for i in xrange(0, n):
        node = graph[i]

        flag = False

        if node.degree > min_degree and node.degree <= max_degree:
            for e in node.adj:
                if graph[e.node_to].degree <= max_degree:
                    flag = True
                    tmp = '%d,,%d,%d,Undirected,%f' % (ctr, e.node_from, e.node_to, e.distance * 10)
                    edges_file.write(tmp)
                    edges_file.write('\n')
                    ctr += 1

            if flag:
                node_file.write('%d,"%s"\n' % (i, node.label ) )                       

    node_file.close()
    edges_file.close()    